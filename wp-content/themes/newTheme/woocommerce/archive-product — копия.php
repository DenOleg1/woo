<?php get_header(); ?> 
<?php get_sidebar(); ?>



<!-- begin category  -->
            <div class="content ">
                <h1 class="title"><span>%Category Name%</span></h1>
                <div class="wallpapers__category">
                    <div class="wallpapers__item action">
                       <img src="<?php bloginfo('template_url'); ?>/images/popular/pw1.jpg" alt="">
                       <div class="wallpapers__info">
                           <div class="wallpapers__info-item wallpapers__title">Улица города Белладжио (Италия)</div>
                           <div class="wallpapers__info-item">
                               <a class="wallpapers__link" href="">Посмотреть</a>
                           </div>
                       </div>
                    </div>
                    <div class="wallpapers__item discount">
                       <img src="<?php bloginfo('template_url'); ?>/images/popular/pw2.jpg" alt="">
                       <div class="wallpapers__info">
                           <div class="wallpapers__info-item wallpapers__title">Карловы вары, чехия</div>
                           <div class="wallpapers__info-item">
                               <a class="wallpapers__link" href="">Посмотреть</a>
                           </div>
                       </div>
                    </div>
                    <div class="wallpapers__item new">
                       <img src="<?php bloginfo('template_url'); ?>/images/popular/pw3.jpg" alt="">
                       <div class="wallpapers__info">
                           <div class="wallpapers__info-item wallpapers__title">Маленькая Венеция на берегу реки Лош. Кольмар, Франция</div>
                           <div class="wallpapers__info-item">
                               <a class="wallpapers__link" href="">Посмотреть</a>
                           </div>
                       </div>
                    </div>
                    <div class="wallpapers__item hit">
                       <img src="<?php bloginfo('template_url'); ?>/images/popular/pw4.jpg" alt="">
                       <div class="wallpapers__info">
                           <div class="wallpapers__info-item wallpapers__title">МОРЕ</div>
                           <div class="wallpapers__info-item">
                               <a class="wallpapers__link" href="">Посмотреть</a>
                           </div>
                       </div>
                    </div>
                    <div class="wallpapers__item">
                       <img src="<?php bloginfo('template_url'); ?>/images/popular/pw5.jpg" alt="">
                       <div class="wallpapers__info">
                           <div class="wallpapers__info-item wallpapers__title">ПЕЙЗАЖ</div>
                           <div class="wallpapers__info-item">
                               <a class="wallpapers__link" href="">Посмотреть</a>
                           </div>
                       </div>
                    </div>
                    <div class="wallpapers__item">
                       <img src="<?php bloginfo('template_url'); ?>/images/popular/pw6.jpg" alt="">
                       <div class="wallpapers__info">
                           <div class="wallpapers__info-item wallpapers__title">ЦВЕТЫ</div>
                           <div class="wallpapers__info-item">
                               <a class="wallpapers__link" href="">Посмотреть</a>
                           </div>
                       </div>
                    </div>
                </div>
            </div>
            <!-- end category -->

<?php if ( have_posts() ) : ?>
		<?php woocommerce_content(); ?>
<?php endif; ?>
<?php get_footer(); ?>