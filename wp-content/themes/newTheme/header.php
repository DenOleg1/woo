<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Home</title>
	<link href="https://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700" rel="stylesheet">
    <?php wp_head(); ?>
</head>
<body>
    <!-- begin wrap  -->
    <div class="wrap">
    	<!-- begin header-top  -->
			<div class="header-top js-header-top">
			    <div class="container">
			        <div class="header-top__item">
			            <a class="header-top__city header__link" href="">Омск</a>
			            <a class="header-top__address header__link" href="">ул. Гагарина, 8/2</a>
			            <a class="header-top__cart header__link" href="/cart">Корзина (<?php echo WC()->cart->get_cart_contents_count(); ?>)</a>
			            <div class="header-top-menu">
			                <ul>
			                    <li class="current-menu-item"><a>АКЦИИ</a></li>
			                    <li><a href="/shop">ФОТООБОИ</a></li>
			                    <li><a href="">ОПЛАТА и ДОСТАВКА</a></li>
			                </ul>
			            </div>
			        </div>
			        <div class="header-top__item">
			            <button class="header-top__phone-call" type="button">Заказать обратный звонок</button>
			            <a class="header-top__number-phone" href="tel:+89087997555">8 <span>908</span> 799-75-55</a>
			        </div>
			    </div>
			</div>
			<!-- end header-top -->

			<!-- begin header  -->
			<div class="header container">
			    <div class="header__item header__logo">
			        <a href="" class="header__logo-link">
			            <?php 
			        		$logo = get_field('logo','options');
			        		if($logo) {
			        			echo '<img src="'.$logo['url'].'" alt="Топ фотообои">';
			        		}
			        	?>
			        </a>
			    </div>
			    <div class="header__item header__menu">
			        <ul>
			            <li class="header__menu_stock"><a href="">Акции</a></li>
			            <li class="current-menu-item header__menu_wallpapers"><a href="">Фотообои</a></li>
			            <li class="header__menu_payment-shipping"><a href="">Оплата и доставка</a></li>
			        </ul>
			    </div>
			</div>
			<!-- end header -->
