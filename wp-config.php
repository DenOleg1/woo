<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'woo');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'R{i@K^mh!cCbSu)OzI`,<JMs8)pF{YetxzeVX8V0|T(tSbAhKOT>&|@ekb3b*^lI');
define('SECURE_AUTH_KEY',  '1,!aQxXq4Nt#=jei*:N9*kRYpyJF}~$Bo3K8$+|XgiOwa~>6!Gf[&BI*L&!{6[P1');
define('LOGGED_IN_KEY',    ' ^W`1;A$M{g@[b:<ksRSBHr7#c9x(zWe`Cagzm7%|05m@<IyGT*eW^~rqLOps,b=');
define('NONCE_KEY',        '~DWx]*_Z+*CC0*J@=]r=O*Vh:.n9%c>~,;D1%)*hIiuO@CI] ,1GEIz`O)_^JR*z');
define('AUTH_SALT',        '9PVWP&J`$9cn}!J%U5aFNqLkiZt,quk{@XK?8N<AyBeVAA^,tbG0W{-~bfGaK*!j');
define('SECURE_AUTH_SALT', 'm5M6x7leu>[L|M>i7i/*|QT#,Tt !iA<%A`#v+C4LF7?.[!$6vLc@`)R@s>?Nmu=');
define('LOGGED_IN_SALT',   'NaLm|l2@LzV[@i9>Ih/d}*Uu8t5@04,/g]0WjuB[m+3~2b]F[I0*=Ymd0$F~${aL');
define('NONCE_SALT',       'G_/ K<.zATKmey}{N&@Sb)>4YEHZ,NBQH[p@Kc2Al@^3G/b2$chk.p3&!xU7dK4j');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
